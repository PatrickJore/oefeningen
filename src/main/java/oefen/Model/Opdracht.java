package oefen.Model;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"opdracht", "cijfer"})})
public class Opdracht {

    public Opdracht() {}

    @Id
    @SequenceGenerator(name="opdracht_id", initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "opdracht_id")
    private long id;

    private String opdracht;
    private Integer cijfer;

    public Opdracht(String opdracht, Integer cijfer) {
        this.opdracht = opdracht;
        this.cijfer = cijfer;
    }

    public Opdracht(String line) {
        this (
                line.split(",")[0],
                Integer.parseInt(line.split(",")[1])
        );
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOpdracht() {
        return opdracht;
    }

    public void setOpdracht(String opdracht) {
        this.opdracht = opdracht;
    }

    public Integer getCijfer() {
        return cijfer;
    }

    public void setCijfer(Integer cijfer) {
        this.cijfer = cijfer;
    }


}
