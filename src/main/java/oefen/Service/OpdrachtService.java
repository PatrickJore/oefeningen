package oefen.Service;

import oefen.Model.Opdracht;
import oefen.Repository.OpdrachtRepository;
import org.springframework.stereotype.Service;

@Service
public class OpdrachtService {

    private OpdrachtRepository opdrachtRepository;

    public OpdrachtService(OpdrachtRepository opdrachtRepository) {
        this.opdrachtRepository = opdrachtRepository;
    }

    public Iterable<Opdracht> getOpdrachten() {
        return opdrachtRepository.findAll();
    }
}
