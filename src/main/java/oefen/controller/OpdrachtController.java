package oefen.controller;

import oefen.Model.Opdracht;
import oefen.Service.OpdrachtService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/opdrachten")
public class OpdrachtController {

    private OpdrachtService service;

    public OpdrachtController(OpdrachtService service) {
        this.service = service;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Opdracht> getOpdrachten() {
        return service.getOpdrachten();
    }
}
