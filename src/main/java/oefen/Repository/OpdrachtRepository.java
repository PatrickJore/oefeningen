package oefen.Repository;

import oefen.Model.Opdracht;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OpdrachtRepository extends CrudRepository<Opdracht, Long> {

}
