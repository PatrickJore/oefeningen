package oefen.config;

import oefen.Model.Opdracht;
import oefen.Repository.OpdrachtRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class OpdrachtenCollector implements ApplicationRunner {

    private OpdrachtRepository opdrachtRepository;

    public OpdrachtenCollector(OpdrachtRepository opdrachtRepository) {
        this.opdrachtRepository = opdrachtRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Path path = Paths.get("src/main/resources/opdrachten.csv");
        Files.lines(path).forEach(
                line -> opdrachtRepository.save(new Opdracht(line))
        );

        opdrachtRepository.findAll()
                .forEach(System.out::println);

    }
}
